﻿using System.Collections.Generic;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Business.Contracts
{
    public interface ICountryManager
    {
        IEnumerable<Country> GetAll();
    }
}