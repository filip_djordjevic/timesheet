﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Business.Contracts
{
    public interface ITeamMemberManager
    {
        void Create(TeamMember teamMember);

        void Update(TeamMember teamMember);

        void Delete(int id);

        IEnumerable<TeamMember> GetBy(int pageNumber);

        IEnumerable<TeamMember> GetAll();
    }
}