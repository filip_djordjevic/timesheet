﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Business.Contracts
{
    public interface ITimeSheetManager
    {
        void Create(DAL.Contracts.Entities.TimeSheet timeSheet);

        void Update(DAL.Contracts.Entities.TimeSheet timeSheet);

        void Delete(int id);

        IEnumerable<DAL.Contracts.Entities.TimeSheet> GetBy(int teamMemberId, int clientId, int projectId, int categoryId, DateTime startDate, DateTime endDate);

        IEnumerable<DAL.Contracts.Entities.TimeSheet> GetAll();
    }
}