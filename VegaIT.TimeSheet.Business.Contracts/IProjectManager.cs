﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Business.Contracts
{
    public interface IProjectManager
    {
        void Create(Project project);

        void Update(Project project);

        void Delete(int id);

        IEnumerable<Project> GetBy(int pageNumber, string startingLetter, string searchText);

        IEnumerable<Project> GetAll();
    }
}