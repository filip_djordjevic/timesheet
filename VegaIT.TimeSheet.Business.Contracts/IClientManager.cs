﻿using System.Collections.Generic;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Business.Contracts
{
    public interface IClientManager
    {
        void Create(Client client);

        void Update(Client client);

        void Delete(int Id);

        IEnumerable<Client> GetBy(int pageNumber, string startingLetter, string searchText);

        IEnumerable<Client> GetAll();
    }
}