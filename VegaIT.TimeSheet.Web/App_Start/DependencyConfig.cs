﻿using Autofac.Integration.Mvc;
using System.Web.Mvc;
using VegaIT.TimeSheet.Infrastructure;

namespace VegaIT.TimeSheet.Web.App_Start
{
    public class DependencyConfig
    {
        public static void RegisterTypes()
        {
            var builder = AutofacConfig.RegisterTypes();
            // Register your MVC controllers. (MvcApplication is the name of
            // the class in Global.asax.)
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}