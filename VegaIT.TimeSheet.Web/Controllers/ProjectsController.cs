﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VegaIT.TimeSheet.Business;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.Common;
using VegaIT.TimeSheet.DAL;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.Web.Helpers;
using VegaIT.TimeSheet.Web.Models;

namespace VegaIT.TimeSheet.Web.Controllers
{
    [Error]
    public class ProjectsController : Controller
    {
        private readonly IProjectManager projectManager;
        private readonly IClientManager clientManager;
        private readonly ITeamMemberManager teamMemberManager;

        public ProjectsController(IClientManager clientManager, IProjectManager projectManager, ITeamMemberManager teamMemberManager)
        {
            this.clientManager = clientManager;
            this.projectManager = projectManager;
            this.teamMemberManager = teamMemberManager;
        }

        [Error]
        // GET: Projects
        public ActionResult Index(int pageNumber = 1, string firstLetter = "", string searchText = "")
        {
            try
            {
                var existingProjects = this.projectManager.GetAll().Select(p => p.Name.ElementAt(0)).ToList();

                IEnumerable<Project> projects = this.projectManager.GetBy(pageNumber, firstLetter, searchText);

                List<Tuple<char, string>> letters = new List<Tuple<char, string>>();

                foreach (char letter in Constants.Letters)
                {
                    if (letter.ToString().Equals(firstLetter))
                        letters.Add(Tuple.Create(letter, Constants.Active));
                    else if (existingProjects.Contains(letter))
                        letters.Add(Tuple.Create(letter, string.Empty));
                    else
                        letters.Add(Tuple.Create(letter, Constants.Disabled));
                }

                ProjectsViewModel projectsViewModel = new ProjectsViewModel
                {
                    Projects = projects,
                    Clients = this.clientManager.GetAll(),
                    TeamMembers = this.teamMemberManager.GetAll(),
                    Letters = letters,
                    Pages = projects.Count() / Constants.PageSize + 1,
                    Letter = firstLetter,
                    SearchText = searchText
                };
                return View(projectsViewModel);
            }
            catch (BusinessException ex)
            {
                throw;
            }
        }

        [Error]
        public ActionResult Create(string name, string description, int leadId, int clientId)
        {
            try
            {
                Project newProject = new Project()
                {
                    Name = name,
                    Description = description,
                    Status = 1,
                    LeadId = leadId,
                    ClientId = clientId
                };
                this.projectManager.Create(newProject);
            }
            catch (BusinessException ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [Error]
        public ActionResult Update(int id, string name, string description, int status, int leadId, int clientId)
        {
            try
            {
                Project projectToUpdate = new Project()
                {
                    Id = id,
                    Name = name,
                    Description = description,
                    Status = status,
                    LeadId = leadId,
                    ClientId = clientId
                };
                this.projectManager.Update(projectToUpdate);
            }
            catch (BusinessException ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [Error]
        public ActionResult Delete(int id)
        {
            try
            {
                this.projectManager.Delete(id);
            }
            catch (BusinessException ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }
    }
}