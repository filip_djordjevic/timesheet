﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.Common;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.Web.Helpers;
using VegaIT.TimeSheet.Web.Models;

namespace VegaIT.TimeSheet.Web.Controllers
{
    [Error]
    public class ClientsController : Controller
    {
        private readonly IClientManager clientManager;
        private readonly ICountryManager countryManager;

        public ClientsController(IClientManager clientManager, ICountryManager countryManager)
        {
            this.clientManager = clientManager;
            this.countryManager = countryManager;
        }

        [Error]
        // GET: Clients
        public ActionResult Index(int pageNumber = 1, string firstLetter = "", string searchText = "")
        {
            try
            {
                var existingClients = this.clientManager.GetAll().Select(c => c.Name.ElementAt(0)).ToList();

                IEnumerable<Client> clients = this.clientManager.GetBy(pageNumber, firstLetter, searchText);

                List<Tuple<char, string>> letters = new List<Tuple<char, string>>();

                foreach (char letter in Constants.Letters)
                {
                    if (letter.ToString().Equals(firstLetter))
                        letters.Add(Tuple.Create(letter, Constants.Active));
                    else if (existingClients.Contains(letter))
                        letters.Add(Tuple.Create(letter, string.Empty));
                    else
                        letters.Add(Tuple.Create(letter, Constants.Disabled));
                }

                ClientsViewModel clientsViewModel = new ClientsViewModel
                {
                    Clients = clients,
                    Countries = this.countryManager.GetAll(),
                    Letters = letters,
                    Pages = clients.Count() / Constants.PageSize + 1,
                    Letter = firstLetter,
                    SearchText = searchText
                };
                return View(clientsViewModel);
            }
            catch (BusinessException ex)
            {
                throw;
            }
        }

        [Error]
        public ActionResult Create(string name, string address, string city, string zipCode, int countryId)
        {
            try
            {
                Client newClient = new Client()
                {
                    Name = name,
                    Address = address,
                    City = city,
                    ZipCode = zipCode,
                    CountryId = countryId
                };
                this.clientManager.Create(newClient);
            }
            catch (BusinessException ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [Error]
        public ActionResult Update(int id, string name, string address, string city, string zipCode, int countryId)
        {
            try
            {
                Client clientToUpdate = new Client()
                {
                    Id = id,
                    Name = name,
                    Address = address,
                    City = city,
                    ZipCode = zipCode,
                    CountryId = countryId
                };
                this.clientManager.Update(clientToUpdate);
            }
            catch (BusinessException ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [Error]
        public ActionResult Delete(int id)
        {
            try
            {
                this.clientManager.Delete(id);
            }
            catch (BusinessException ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }
    }
}