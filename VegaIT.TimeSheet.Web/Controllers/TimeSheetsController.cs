﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.Common;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;
using VegaIT.TimeSheet.Web.Helpers;
using VegaIT.TimeSheet.Web.Models;

namespace VegaIT.TimeSheet.Web.Controllers
{
    [Error]
    public class TimeSheetsController : Controller
    {
        private readonly ITimeSheetManager timeSheetManager;
        private readonly ITeamMemberManager teamMemberManager;
        private readonly IClientManager clientManager;
        private readonly IProjectManager projectManager;
        private readonly ICategoryRepository categoryRepository;
        private readonly IProjectCategoryRepository projectCategoryRepository;

        public TimeSheetsController(ITimeSheetManager timeSheetManager, IClientManager clientManager, IProjectManager projectManager,
                                    ICategoryRepository categoryRepository, IProjectCategoryRepository projectCategoryRepository, ITeamMemberManager teamMemberManager)
        {
            this.timeSheetManager = timeSheetManager;
            this.clientManager = clientManager;
            this.projectManager = projectManager;
            this.categoryRepository = categoryRepository;
            this.projectCategoryRepository = projectCategoryRepository;
            this.teamMemberManager = teamMemberManager;
        }

        [Error]
        // GET: TimeSheets
        public ActionResult Index(int year, int month, int day)
        {
            try
            {
                DateTime currentDate = new DateTime(year, month, day);
                List<Tuple<int, int, int, string, string>> DaysOfWeek = new List<Tuple<int, int, int, string, string>>();
                decimal dayTotalTime = 0;

                var timeSheets = this.timeSheetManager.GetAll().Where(ts => ts.Date.Equals(currentDate));
                var clients = this.clientManager.GetAll();
                var projects = this.projectManager.GetAll();
                var categories = this.categoryRepository.GetAll();
                var projectCategories = this.projectCategoryRepository.GetAll();

                if (currentDate.DayOfWeek.Equals(DayOfWeek.Monday))
                {
                    CurrentDay(year, month, day, ref DaysOfWeek);
                    DaysAfter(year, month, day, 6, ref DaysOfWeek);
                }
                else if (currentDate.DayOfWeek.Equals(DayOfWeek.Tuesday))
                {
                    DaysBefore(year, month, day, 1, ref DaysOfWeek);
                    CurrentDay(year, month, day, ref DaysOfWeek);
                    DaysAfter(year, month, day, 5, ref DaysOfWeek);
                }
                else if (currentDate.DayOfWeek.Equals(DayOfWeek.Wednesday))
                {
                    DaysBefore(year, month, day, 2, ref DaysOfWeek);
                    CurrentDay(year, month, day, ref DaysOfWeek);
                    DaysAfter(year, month, day, 4, ref DaysOfWeek);
                }
                else if (currentDate.DayOfWeek.Equals(DayOfWeek.Thursday))
                {
                    DaysBefore(year, month, day, 3, ref DaysOfWeek);
                    CurrentDay(year, month, day, ref DaysOfWeek);
                    DaysAfter(year, month, day, 3, ref DaysOfWeek);
                }
                else if (currentDate.DayOfWeek.Equals(DayOfWeek.Friday))
                {
                    DaysBefore(year, month, day, 4, ref DaysOfWeek);
                    CurrentDay(year, month, day, ref DaysOfWeek);
                    DaysAfter(year, month, day, 2, ref DaysOfWeek);
                }
                else if (currentDate.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    DaysBefore(year, month, day, 5, ref DaysOfWeek);
                    CurrentDay(year, month, day, ref DaysOfWeek);
                    DaysAfter(year, month, day, 1, ref DaysOfWeek);
                }
                else
                {
                    DaysBefore(year, month, day, 6, ref DaysOfWeek);
                    CurrentDay(year, month, day, ref DaysOfWeek);
                }

                if (timeSheets != null)
                {
                    foreach (DAL.Contracts.Entities.TimeSheet ts in timeSheets)
                    {
                        dayTotalTime += ts.Time;
                    }
                }

                TimeSheetDetailViewModel timeSheetsDetailViewModel = new TimeSheetDetailViewModel
                {
                    CalendarWeek = DaysOfWeek,
                    CurrentMonth = month,
                    CurrentYear = year,
                    CurrentDay = day,
                    Months = Constants.Months,
                    TimeSheets = timeSheets,
                    Clients = clients,
                    Projects = projects,
                    Categories = categories,
                    DayTotalTime = dayTotalTime,
                    ProjectCategories = projectCategories,
                    WeekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(currentDate, CalendarWeekRule.FirstDay, DayOfWeek.Sunday)
                };
                return View(timeSheetsDetailViewModel);
            }
            catch (BusinessException ex)
            {
                throw;
            }
        }

        [Error]
        public ActionResult Create(int year, int month, int day)
        {
            try
            {
                int categoryId;
                int projectId;
                int clientId;
                decimal time;
                decimal overtime;
                int projectCategoryId = -1;

                string[] clients = Request.Form.GetValues("clientId");
                string[] projects = Request.Form.GetValues("projectId");
                string[] categories = Request.Form.GetValues("categoryId");
                string[] descriptions = Request.Form.GetValues("description");
                string[] times = Request.Form.GetValues("time");
                string[] overtimes = Request.Form.GetValues("overtime");

                var teamMember = this.teamMemberManager.GetAll().FirstOrDefault();

                for (int i = 0; i < clients.Count(); i++)
                {
                    if (!int.TryParse(categories[i], out categoryId))
                    {
                        continue;
                    }
                    if (!int.TryParse(projects[i], out projectId))
                    {
                        continue;
                    }
                    if (!int.TryParse(clients[i], out clientId))
                    {
                        continue;
                    }
                    if (!decimal.TryParse(times[i], out time))
                    {
                        continue;
                    }
                    if (overtimes[i] == "")
                    {
                        overtime = 0;
                    }
                    else if (!decimal.TryParse(overtimes[i], out overtime))
                    {
                        continue;
                    }

                    if (this.projectCategoryRepository.GetAll().Where(pc => pc.CategoryId == categoryId && pc.ProjectId == projectId).Count() > 0)
                    {
                        projectCategoryId = this.projectCategoryRepository.GetAll().Where(pc => pc.CategoryId == categoryId && pc.ProjectId == projectId).First().Id;
                    }
                    else
                    {
                        ProjectCategory newProjectCategory = new ProjectCategory()
                        {
                            CategoryId = categoryId,
                            ProjectId = projectId
                        };
                        this.projectCategoryRepository.Add(newProjectCategory);
                    }

                    if (projectCategoryId != -1)
                    {
                        DAL.Contracts.Entities.TimeSheet newTimeSheet = new DAL.Contracts.Entities.TimeSheet()
                        {
                            ClientId = clientId,
                            Description = descriptions[i],
                            Time = time,
                            Overtime = overtime,
                            Date = new DateTime(year, month, day),
                            TeamMemberId = teamMember.Id,
                            ProjectCategoryId = projectCategoryId
                        };
                        this.timeSheetManager.Create(newTimeSheet);
                    }
                    else
                    {
                        DAL.Contracts.Entities.TimeSheet newTimeSheet = new DAL.Contracts.Entities.TimeSheet()
                        {
                            ClientId = clientId,
                            Description = descriptions[i],
                            Time = time,
                            Overtime = overtime,
                            Date = new DateTime(year, month, day),
                            TeamMemberId = teamMember.Id,
                            ProjectCategoryId = projectCategoryRepository.GetAll().LastOrDefault().Id
                        };
                        this.timeSheetManager.Create(newTimeSheet);
                    }
                }
            }
            catch (BusinessException ex)
            {
                throw;
            }

            return RedirectToAction("Index", new { year = year, month = month, day = day });
        }

        [Error]
        public void DaysAfter(int year, int month, int day, int dayOffset, ref List<Tuple<int, int, int, string, string>> daysOfWeek)
        {
            int startDay = 0;
            for (int i = day + 1; i <= day + dayOffset; i++)
            {
                if (i > DateTime.DaysInMonth(year, month))
                {
                    if (month != 12)
                    {
                        DateTime newDay = new DateTime(year, month + 1, startDay += 1);
                        if (newDay > DateTime.Now)
                        {
                            daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month + 1, startDay, newDay.DayOfWeek.ToString(), Constants.Disabled));
                        }
                        else
                        {
                            daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month + 1, startDay, newDay.DayOfWeek.ToString(), ""));
                        }
                    }
                    else
                    {
                        DateTime newDay = new DateTime(year, 1, startDay += 1);
                        if (newDay > DateTime.Now)
                        {
                            daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, 1, startDay, newDay.DayOfWeek.ToString(), Constants.Disabled));
                        }
                        else
                        {
                            daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, 1, startDay, newDay.DayOfWeek.ToString(), ""));
                        }
                    }
                }
                else
                {
                    DateTime newDay = new DateTime(year, month, i);
                    if (newDay > DateTime.Now)
                    {
                        daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month, i, newDay.DayOfWeek.ToString(), Constants.Disabled));
                    }
                    else
                    {
                        daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month, i, newDay.DayOfWeek.ToString(), ""));
                    }
                }
            }
        }

        [Error]
        public void DaysBefore(int year, int month, int day, int dayOffset, ref List<Tuple<int, int, int, string, string>> daysOfWeek)
        {
            int dayCounter = 0;
            for (int i = day - dayOffset; i <= day - 1; i++)
            {
                dayCounter++;
                if (i < 1)
                {
                    if (month != 1)
                    {
                        int startDay = DateTime.DaysInMonth(year, month - 1);
                        DateTime newDay = new DateTime(year, month - 1, startDay - (dayOffset - dayCounter));
                        if (newDay > DateTime.Now)
                        {
                            daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month - 1, startDay - (dayOffset - dayCounter), newDay.DayOfWeek.ToString(), Constants.Disabled));
                        }
                        else
                        {
                            daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month - 1, startDay - (dayOffset - dayCounter), newDay.DayOfWeek.ToString(), ""));
                        }
                    }
                    else
                    {
                        int startDay = DateTime.DaysInMonth(year, 12);
                        DateTime newDay = new DateTime(year, 12, startDay - (dayOffset - dayCounter));
                        if (newDay > DateTime.Now)
                        {
                            daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, 12, startDay - (dayOffset - dayCounter), newDay.DayOfWeek.ToString(), Constants.Disabled));
                        }
                        else
                        {
                            daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, 12, startDay - (dayOffset - dayCounter), newDay.DayOfWeek.ToString(), ""));
                        }
                    }
                }
                else
                {
                    DateTime newDay = new DateTime(year, month, i);
                    if (newDay > DateTime.Now)
                    {
                        daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month, i, newDay.DayOfWeek.ToString(), Constants.Disabled));
                    }
                    else
                    {
                        daysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month, i, newDay.DayOfWeek.ToString(), ""));
                    }
                }
            }
        }

        [Error]
        private void CurrentDay(int year, int month, int day, ref List<Tuple<int, int, int, string, string>> DaysOfWeek)
        {
            DateTime newDay = new DateTime(year, month, day);
            if (newDay > DateTime.Now)
            {
                DaysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month, day, newDay.DayOfWeek.ToString(), Constants.Disabled));
            }
            else
            {
                DaysOfWeek.Add(new Tuple<int, int, int, string, string>(year, month, day, newDay.DayOfWeek.ToString(), Constants.Active));
            }
        }

        [Error]
        private JsonResult GetProjects(int clientId)
        {
            var projects = this.projectManager.GetAll().Where(p => p.ClientId == clientId);

            return Json(projects, JsonRequestBehavior.AllowGet);
        }

        [Error]
        private JsonResult GetCategories(int projectId)
        {
            var projectCategories = this.projectCategoryRepository.GetAll().Where(pc => pc.ProjectId == projectId);

            List<Category> categories = new List<Category>();

            foreach(ProjectCategory pc in projectCategories)
            {
                categories.Add(this.categoryRepository.GetBy(pc.CategoryId));
            }

            return Json(categories, JsonRequestBehavior.AllowGet); ;
        }
    }
}