﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;
using VegaIT.TimeSheet.Web.Helpers;
using VegaIT.TimeSheet.Web.Models;

namespace VegaIT.TimeSheet.Web.Controllers
{
    [Error]
    public class ReportsController : Controller
    {
        private readonly ITimeSheetManager timeSheetManager;
        private readonly ITeamMemberManager teamMemberManager;
        private readonly IClientManager clientManager;
        private readonly IProjectManager projectManager;
        private readonly ICategoryRepository categoryRepository;

        public ReportsController(ITimeSheetManager timeSheetManager, IClientManager clientManager, IProjectManager projectManager,
                                    ICategoryRepository categoryRepository, ITeamMemberManager teamMemberManager)
        {
            this.timeSheetManager = timeSheetManager;
            this.clientManager = clientManager;
            this.projectManager = projectManager;
            this.categoryRepository = categoryRepository;
            this.teamMemberManager = teamMemberManager;
        }

        [Error]
        // GET: Reports
        public ActionResult Index(int teamMemberId = 0, int clientId = 0, int projectId = 0, int categoryId = 0, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                List<Tuple<DateTime, string, string, string, string, decimal>> reportResults = new List<Tuple<DateTime, string, string, string, string, decimal>>();
                decimal reportTotalTime = 0;

                var teamMembers = this.teamMemberManager.GetAll();
                var clients = this.clientManager.GetAll();
                var projects = this.projectManager.GetAll();
                var categories = this.categoryRepository.GetAll();

                IEnumerable<DAL.Contracts.Entities.TimeSheet> resultingTimeSheets = new List<DAL.Contracts.Entities.TimeSheet>();
                if (startDate.HasValue && endDate.HasValue)
                {
                    resultingTimeSheets = this.timeSheetManager.GetBy(teamMemberId, clientId, projectId, categoryId, startDate.Value, endDate.Value);
                }

                foreach (DAL.Contracts.Entities.TimeSheet timeSheet in resultingTimeSheets)
                {
                    reportResults.Add(new Tuple<DateTime, string, string, string, string, decimal>(timeSheet.Date,
                                        teamMembers.Where(tm => tm.Id == teamMemberId).FirstOrDefault().Name,
                                        projects.Where(p => p.Id == projectId).FirstOrDefault().Name,
                                        categories.Where(c => c.Id == categoryId).FirstOrDefault().Name,
                                        timeSheet.Description, timeSheet.Time));
                    reportTotalTime += timeSheet.Time;
                }

                ReportsViewModel reportsViewModel = new ReportsViewModel
                {
                    TeamMembers = teamMembers,
                    Clients = clients,
                    Projects = projects,
                    Categories = categories,
                    ResultTimeSheets = reportResults,
                    TotalTime = reportTotalTime
                };
                return View(reportsViewModel);
            }
            catch (BusinessException ex)
            {
                throw;
            }
        }
    }
}