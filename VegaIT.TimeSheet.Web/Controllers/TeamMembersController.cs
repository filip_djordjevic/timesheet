﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VegaIT.TimeSheet.Business;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.Common;
using VegaIT.TimeSheet.DAL;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.Web.Helpers;
using VegaIT.TimeSheet.Web.Models;

namespace VegaIT.TimeSheet.Web.Controllers
{
    [Error]
    public class TeamMembersController : Controller
    {
        private readonly ITeamMemberManager teamMemberManager;

        public TeamMembersController(ITeamMemberManager teamMemberManager)
        {
            this.teamMemberManager = teamMemberManager;
        }

        [Error]
        // GET: TeamMembers
        public ActionResult Index(int pageNumber = 1)
        {
            try
            {
                IEnumerable<TeamMember> teamMembers = this.teamMemberManager.GetBy(pageNumber);

                TeamMembersViewModel teamMembersViewModel = new TeamMembersViewModel()
                {
                    TeamMembers = teamMemberManager.GetAll(),
                    Pages = teamMembers.Count() / Constants.PageSize + 1
                };
                return View(teamMembersViewModel);
            }
            catch (BusinessException ex)
            {
                throw;
            }
        }

        [Error]
        public ActionResult Create(string name, decimal hoursPerWeek, string userName, string email, int status, int role)
        {
            try
            {
                TeamMember newTeamMember = new TeamMember()
                {
                    Name = name,
                    HoursPerWeek = hoursPerWeek,
                    UserName = userName,
                    Email = email
                };
                if(status==1)
                {
                    newTeamMember.IsActive = true;
                }
                else
                {
                    newTeamMember.IsActive = false;
                }
                if (role == 1)
                {
                    newTeamMember.IsAdmin = true;
                }
                else
                {
                    newTeamMember.IsAdmin = false;
                }
                this.teamMemberManager.Create(newTeamMember);
            }
            catch (BusinessException ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [Error]
        public ActionResult Update(int id, string name, decimal hoursPerWeek, string userName, string email, int status, int role)
        {
            try
            {
                TeamMember teamMemberToUpdate = new TeamMember()
                {
                    Id = id,
                    Name = name,
                    HoursPerWeek = hoursPerWeek,
                    UserName = userName,
                    Email = email
                };
                if (status == 1)
                {
                    teamMemberToUpdate.IsActive = true;
                }
                else
                {
                    teamMemberToUpdate.IsActive = false;
                }
                if (role == 1)
                {
                    teamMemberToUpdate.IsAdmin = true;
                }
                else
                {
                    teamMemberToUpdate.IsAdmin = false;
                }
                this.teamMemberManager.Update(teamMemberToUpdate);
            }
            catch (BusinessException ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [Error]
        public ActionResult Delete(int id)
        {
            try
            {
                this.teamMemberManager.Delete(id);
            }
            catch (BusinessException ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }
    }
}