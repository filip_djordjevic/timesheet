﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.Common;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.Web.Helpers;
using VegaIT.TimeSheet.Web.Models;

namespace VegaIT.TimeSheet.Web.Controllers
{
    [Error]
    //[Authorize]
    public class HomeController : Controller
    {
        private readonly ITimeSheetManager timeSheetManager;
        private readonly ITeamMemberManager teamMemberManager;

        public HomeController(ITimeSheetManager timeSheetManager, ITeamMemberManager teamMemberManager)
        {
            this.timeSheetManager = timeSheetManager;
            this.teamMemberManager = teamMemberManager;
        }

        [Error]
        //[Authorize(Roles = "Admin,Worker")]
        public ActionResult Index(int month = 7, int year = 2017)
        {
            try
            {
                bool nextYear = false;
                bool lastYear = false;
                decimal sumTime = 0;

                int numberOfDaysCurrentMonth = DateTime.DaysInMonth(year, month);

                if (month == 1)
                {
                    int numberOfDaysPreviousMonth = DateTime.DaysInMonth(year - 1, 12);
                    lastYear = true;
                }
                else
                {
                    int numberOfDaysPreviousMonth = DateTime.DaysInMonth(year, month - 1);
                }
                if (month == 12)
                {
                    int numberOfDaysPreviousMonth = DateTime.DaysInMonth(year + 1, 1);
                    nextYear = true;
                }
                else
                {
                    int numberOfDaysNextMonth = DateTime.DaysInMonth(year, month + 1);
                }

                var timeSheets = this.timeSheetManager.GetAll();
                var teamMember = this.teamMemberManager.GetAll().FirstOrDefault();

                List<Tuple<int, int, int, decimal, string>> DaysOfMonth = new List<Tuple<int, int, int, decimal, string>>();

                DateTime First = new DateTime(year, month, 1);
                DateTime Last = new DateTime(year, month, DateTime.DaysInMonth(year, month));

                if (First.DayOfWeek.Equals(DayOfWeek.Tuesday))
                {
                    MonthBefore(lastYear, year, month, 0, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (First.DayOfWeek.Equals(DayOfWeek.Wednesday))
                {
                    MonthBefore(lastYear, year, month, 1, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (First.DayOfWeek.Equals(DayOfWeek.Thursday))
                {
                    MonthBefore(lastYear, year, month, 2, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (First.DayOfWeek.Equals(DayOfWeek.Friday))
                {
                    MonthBefore(lastYear, year, month, 3, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (First.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    MonthBefore(lastYear, year, month, 4, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (First.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    MonthBefore(lastYear, year, month, 5, teamMember, timeSheets, ref DaysOfMonth);
                }

                for (int i = 1; i <= numberOfDaysCurrentMonth; i++)
                {
                    DateTime day = new DateTime(year, month, i);
                    if (day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday)
                    {
                        if (day > DateTime.Now)
                        {
                            DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, 0, Constants.Disable));
                        }
                        else
                        {
                            DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, 0, ""));
                        }
                    }
                    else
                    {
                        if (day > DateTime.Now)
                        {
                            DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, 0, Constants.Disable));
                        }
                        else
                        {
                            var time = timeSheets.Where(ts => ts.Date.Equals(day)).FirstOrDefault();
                            if (time == null)
                            {
                                DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, 0, ""));
                            }
                            else
                            {
                                if (time.Time >= teamMember.HoursPerWeek / 5)
                                {
                                    sumTime += time.Time;
                                    DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, time.Time, Constants.Positive));
                                }
                                else
                                {
                                    sumTime += time.Time;
                                    DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, time.Time, Constants.Negative));
                                }
                            } 
                        }
                    }
                }

                if (Last.DayOfWeek.Equals(DayOfWeek.Monday))
                {
                    MonthAfter(nextYear, year, month, 6, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (Last.DayOfWeek.Equals(DayOfWeek.Tuesday))
                {
                    MonthAfter(nextYear, year, month, 5, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (Last.DayOfWeek.Equals(DayOfWeek.Wednesday))
                {
                    MonthAfter(nextYear, year, month, 4, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (Last.DayOfWeek.Equals(DayOfWeek.Thursday))
                {
                    MonthAfter(nextYear, year, month, 3, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (Last.DayOfWeek.Equals(DayOfWeek.Friday))
                {
                    MonthAfter(nextYear, year, month, 2, teamMember, timeSheets, ref DaysOfMonth);
                }
                else if (Last.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    MonthAfter(nextYear, year, month, 1, teamMember, timeSheets, ref DaysOfMonth);
                }

                TimeSheetsViewModel timeSheetsViewModel = new TimeSheetsViewModel
                {
                    CalendarMonth = DaysOfMonth,
                    CurrentMonth = month,
                    CurrentYear = year,
                    Months = Constants.Months,
                    TotalTime = sumTime
                };
                return View(timeSheetsViewModel);
            }
            catch (BusinessException ex)
            {
                throw;
            }
        }


        public ActionResult Error()
        {
            return View();
        }

        [Error]
        public void MonthBefore(bool lastYear, int year, int month, int dayOffset, TeamMember teamMember, IEnumerable<DAL.Contracts.Entities.TimeSheet> timeSheets, ref List<Tuple<int, int, int, decimal, string>> DaysOfMonth)
        {
            if (lastYear)
            {
                for (int i = DateTime.DaysInMonth(year - 1, 12) - dayOffset; i <= DateTime.DaysInMonth(year - 1, 12); i++)
                {
                    DateTime day = new DateTime(year - 1, 12, i);
                    if (day > DateTime.Now)
                    {
                        DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, 0, Constants.Disable));
                    }
                    else
                    {
                        var time = timeSheets.Where(ts => ts.Date.Equals(day)).FirstOrDefault();
                        if (time == null)
                        {
                            DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year - 1, 12, i, 0, Constants.Previous));
                        }
                        else
                        {
                            if (time.Time >= teamMember.HoursPerWeek / 5)
                            {
                                DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year - 1, 12, i, time.Time, Constants.Positive + " " + Constants.Previous));
                            }
                            else
                            {
                                DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year - 1, 12, i, time.Time, Constants.Negative + " " + Constants.Previous));
                            }
                        }
                    }
                }
            }
            else
            {
                for (int i = DateTime.DaysInMonth(year, month - 1) - dayOffset; i <= DateTime.DaysInMonth(year, month - 1); i++)
                {
                    DateTime day = new DateTime(year, month - 1, i);
                    if (day > DateTime.Now)
                    {
                        DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, 0, Constants.Disable));
                    }
                    else
                    {
                        var time = timeSheets.Where(ts => ts.Date.Equals(day)).FirstOrDefault();
                        if (time == null)
                        {
                            DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month - 1, i, 0, Constants.Previous));
                        }
                        else
                        {
                            if (time.Time > teamMember.HoursPerWeek / 5)
                            {
                                DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month - 1, i, time.Time, Constants.Positive + " " + Constants.Previous));
                            }
                            else
                            {
                                DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month - 1, i, time.Time, Constants.Negative + " " + Constants.Previous));
                            }
                        }
                    }
                }
            }
        }

        [Error]
        public void MonthAfter(bool nextYear, int year, int month, int dayOffset, TeamMember teamMember, IEnumerable<DAL.Contracts.Entities.TimeSheet> timeSheets, ref List<Tuple<int, int, int, decimal, string>> DaysOfMonth)
        {
            if (nextYear)
            {
                for (int i = 1; i <= dayOffset; i++)
                {
                    DateTime day = new DateTime(year + 1, 1, i);
                    if (day > DateTime.Now)
                    {
                        DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, 0, Constants.Disable));
                    }
                    else
                    {
                        var time = timeSheets.Where(ts => ts.Date.Equals(day)).FirstOrDefault();
                        if (time == null)
                        {
                            DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year + 1, 1, i, 0, ""));
                        }
                        else
                        {
                            if (time.Time >= teamMember.HoursPerWeek / 5)
                            {
                                DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year + 1, 1, i, time.Time, Constants.Positive + " " + Constants.Previous));
                            }
                            else
                            {
                                DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year + 1, 1, i, time.Time, Constants.Negative + " " + Constants.Previous));
                            }
                        }
                    }
                }
            }
            else
            {
                for (int i = 1; i <= dayOffset; i++)
                {
                    DateTime day = new DateTime(year, month + 1, i);
                    if (day > DateTime.Now)
                    {
                        DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month, i, 0, Constants.Disable));
                    }
                    else
                    {
                        var time = timeSheets.Where(ts => ts.Date.Equals(day)).FirstOrDefault();
                        if (time == null)
                        {
                            DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year + 1, 1, i, 0, ""));
                        }
                        else
                        {
                            if (time.Time > teamMember.HoursPerWeek / 5)
                            {
                                DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month + 1, i, time.Time, Constants.Positive + " " + Constants.Previous));
                            }
                            else
                            {
                                DaysOfMonth.Add(new Tuple<int, int, int, decimal, string>(year, month + 1, i, time.Time, Constants.Negative + " " + Constants.Previous));
                            }
                        }
                    }
                }
            }
        }
    }
}