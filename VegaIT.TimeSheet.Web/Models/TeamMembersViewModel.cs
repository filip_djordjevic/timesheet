﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Web.Models
{
    public class TeamMembersViewModel
    {
        public IEnumerable<TeamMember> TeamMembers { get; set; }
        public int Pages { get; set; }
    }
}