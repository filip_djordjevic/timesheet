﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Web.Models
{
    public class ProjectsViewModel
    {
        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<Tuple<char, string>> Letters { get; set; }
        public IEnumerable<Client> Clients { get; set; }
        public IEnumerable<TeamMember> TeamMembers { get; set; }
        public int Pages { get; set; }
        public string Letter { get; set; }
        public string SearchText { get; set; }
        public int CurrentPage { get; set; }
    }
}