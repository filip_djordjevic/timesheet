﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Web.Models
{
    public class ReportsViewModel
    {
        public List<Tuple<DateTime, string, string, string, string, decimal>> ResultTimeSheets { get; set; }
        public decimal TotalTime { get; set; }
        public IEnumerable<Client> Clients { get; set; }
        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<TeamMember> TeamMembers { get; set; }
    }
}