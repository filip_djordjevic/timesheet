﻿using System;
using System.Collections.Generic;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Web.Models
{
    public class ClientsViewModel
    {
        public IEnumerable<Client> Clients { get; set; }
        public IEnumerable<Tuple<char, string>> Letters { get; set; }
        public IEnumerable<Country> Countries { get; set; }
        public int Pages { get; set; }
        public string Letter { get; set; }
        public string SearchText { get; set; }
        public int CurrentPage { get; set; }
    }
}