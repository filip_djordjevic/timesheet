﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Web.Models
{
    public class TimeSheetsViewModel
    {
        public List<Tuple<int, int, int, decimal, string>> CalendarMonth { get; set; }
        public Dictionary<int, string> Months { get; set; }
        public int CurrentYear { get; set; }
        public int CurrentMonth { get; set; }
        public decimal TotalTime { get; set; }
    }
}