﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.Web.Models
{
    public class TimeSheetDetailViewModel
    {
        public List<Tuple<int, int, int, string, string>> CalendarWeek { get; set; }
        public int CurrentYear { get; set; }
        public int CurrentMonth { get; set; }
        public int CurrentDay { get; set; }
        public Dictionary<int, string> Months { get; set; }
        public IEnumerable<DAL.Contracts.Entities.TimeSheet> TimeSheets { get; set; }
        public IEnumerable<Client> Clients { get; set; }
        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<ProjectCategory> ProjectCategories { get; set; }
        public decimal DayTotalTime { get; set; }
        public int WeekNumber { get; set; }
    }
}