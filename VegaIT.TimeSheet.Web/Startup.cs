﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VegaIT.TimeSheet.Web.Startup))]
namespace VegaIT.TimeSheet.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
