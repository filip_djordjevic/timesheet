﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VegaIT.TimeSheet.Common
{
    public class Constants
    {
        public static readonly int PageSize = 10;

        public static readonly IEnumerable<char> Letters = Enumerable.Range('A', 26).Select(Convert.ToChar);

        public static readonly string Disabled = "disabled";

        public static readonly string Active = "active";

        public static readonly string Disable = "disable";

        public static readonly string Positive = "positive";

        public static readonly string Negative = "negative";

        public static readonly string Previous = "previous";

        public static readonly Dictionary<int, string> Months = new Dictionary<int, string>
        {
            { 1, "January" },
            { 2, "February" },
            { 3, "March" },
            { 4, "April" },
            { 5, "May" },
            { 6, "June" },
            { 7, "July" },
            { 8, "August" },
            { 9, "September" },
            { 10, "October" },
            { 11, "November" },
            { 12, "December" }
        };
    }
}