﻿using System;
using System.Collections.Generic;
using System.Linq;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.DAL.InMemory
{
    public class ClientRepository : IClientRepository
    {
        private IList<Client> clients = new List<Client>
        {
            new Client { Id = 1, Name = "Vega IT", Address = "Novosadskog sajma 2", ZipCode = "21000", City = "Novi Sad", CountryId = 1}
        };

        public void Add(Client client)
        {
            this.clients.Add(client);
        }

        public IEnumerable<Client> GetAll()
        {
            return this.clients;
        }

        public Client GetBy(int id)
        {
            return this.clients.SingleOrDefault(c => c.Id == id);
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Client client)
        {
            throw new NotImplementedException();
        }
    }
}