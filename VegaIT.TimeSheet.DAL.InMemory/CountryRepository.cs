﻿using System;
using System.Collections.Generic;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.DAL.InMemory
{
    public class CountryRepository : ICountryRepository
    {
        private IList<Country> countries = new List<Country> { new Country { Id = 1, Name = "Serbia" } };

        public void Add(Country country)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Country> GetAll()
        {
            return this.countries;
        }

        public Country GetBy(int id)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Country country)
        {
            throw new NotImplementedException();
        }
    }
}