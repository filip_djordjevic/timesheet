﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.DAL
{
    public class TimeSheetRepository : ITimeSheetRepository
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["TimeSheetDb"].ConnectionString;

        public IEnumerable<Contracts.Entities.TimeSheet> GetAll()
        {
            string query = "select Id, Description, Time, Overtime, Date, TeamMemberId, ClientId, ProjectCategoryId from TimeSheets";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var timeSheets = new List<Contracts.Entities.TimeSheet>();
                            while (reader.Read())
                            {
                                var timeSheet = new Contracts.Entities.TimeSheet()
                                {
                                    Id = Convert.ToInt32(reader.GetValue(0)),
                                    Description = Convert.ToString(reader.GetValue(1)),
                                    Time = Convert.ToDecimal(reader.GetValue(2)),
                                    Overtime = Convert.ToDecimal(reader.GetValue(3)),
                                    Date = Convert.ToDateTime(reader.GetValue(4)),
                                    TeamMemberId = Convert.ToInt32(reader.GetValue(5)),
                                    ClientId = Convert.ToInt32(reader.GetValue(6)),
                                    ProjectCategoryId = Convert.ToInt32(reader.GetValue(7))
                                };
                                timeSheets.Add(timeSheet);
                            }
                            return timeSheets;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public Contracts.Entities.TimeSheet GetBy(int id)
        {
            var timeSheet = new Contracts.Entities.TimeSheet();
            string query = "select Id, Description, Time, Overtime, Date, TeamMemberId, ClientId, ProjectCategoryId from TimeSheets where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                timeSheet.Id = Convert.ToInt32(reader.GetValue(0));
                                timeSheet.Description = Convert.ToString(reader.GetValue(1));
                                timeSheet.Time = Convert.ToDecimal(reader.GetValue(2));
                                timeSheet.Overtime = Convert.ToDecimal(reader.GetValue(3));
                                timeSheet.Date = Convert.ToDateTime(reader.GetValue(4));
                                timeSheet.TeamMemberId = Convert.ToInt32(reader.GetValue(5));
                                timeSheet.ClientId = Convert.ToInt32(reader.GetValue(6));
                                timeSheet.ProjectCategoryId = Convert.ToInt32(reader.GetValue(7));
                            }
                            return timeSheet;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Add(Contracts.Entities.TimeSheet timeSheet)
        {
            string query = "insert into TimeSheets(Description, Time, Overtime, Date, TeamMemberId, ClientId, ProjectCategoryId) values (@description, @time, @overtime, @date, @teamMemberId, @clientId, @projectCategoryId)";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@description", timeSheet.Description);
                        command.Parameters.AddWithValue("@time", timeSheet.Time);
                        command.Parameters.AddWithValue("@overtime", timeSheet.Overtime);
                        command.Parameters.AddWithValue("@date", timeSheet.Date);
                        command.Parameters.AddWithValue("@teamMemberId", timeSheet.TeamMemberId);
                        command.Parameters.AddWithValue("@clientId", timeSheet.ClientId);
                        command.Parameters.AddWithValue("@projectCategoryId", timeSheet.ProjectCategoryId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Update(Contracts.Entities.TimeSheet timeSheet)
        {
            string query = "update TimeSheets set Description = @description, Time = @time, Overtime = @overtime, Date = @date, TeamMemberId = @teamMemberId, ClientId = @clientId, ProjectCategoryId = @projectCategoryId where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", timeSheet.Id);
                        command.Parameters.AddWithValue("@description", timeSheet.Description);
                        command.Parameters.AddWithValue("@time", timeSheet.Time);
                        command.Parameters.AddWithValue("@overtime", timeSheet.Overtime);
                        command.Parameters.AddWithValue("@date", timeSheet.Date);
                        command.Parameters.AddWithValue("@teamMemberId", timeSheet.TeamMemberId);
                        command.Parameters.AddWithValue("@clientId", timeSheet.ClientId);
                        command.Parameters.AddWithValue("@projectCategoryId", timeSheet.ProjectCategoryId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Remove(int id)
        {
            string query = "delete from TimeSheets where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }
    }
}
