﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.DAL
{
    public class ProjectCategoryRepository : IProjectCategoryRepository
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["TimeSheetDb"].ConnectionString;

        public IEnumerable<ProjectCategory> GetAll()
        {
            string query = "select Id, CategoryId, ProjectId from ProjectCategory";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var projectCategories = new List<ProjectCategory>();
                            while (reader.Read())
                            {
                                var projectCategory = new ProjectCategory
                                {
                                    Id = Convert.ToInt32(reader.GetValue(0)),
                                    CategoryId = Convert.ToInt32(reader.GetValue(1)),
                                    ProjectId = Convert.ToInt32(reader.GetValue(2))
                                };
                                projectCategories.Add(projectCategory);
                            }
                            return projectCategories;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public ProjectCategory GetBy(int id)
        {
            var projectCategory = new ProjectCategory();
            string query = "select Id, CategoryId, ProjectId from ProjectCategory where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                projectCategory.Id = Convert.ToInt32(reader.GetValue(0));
                                projectCategory.CategoryId = Convert.ToInt32(reader.GetValue(1));
                                projectCategory.ProjectId = Convert.ToInt32(reader.GetValue(2));
                            }
                            return projectCategory;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Add(ProjectCategory projectCategory)
        {
            string query = "insert into ProjectCategory(CategoryId, ProjectId) values (@categoryId, @projectId)";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@categoryId", projectCategory.CategoryId);
                        command.Parameters.AddWithValue("@projectId", projectCategory.ProjectId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Update(ProjectCategory projectCategory)
        {
            string query = "update ProjectCategory set CategoryId = @categoryId, ProjectId = @projectId where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", projectCategory.Id);
                        command.Parameters.AddWithValue("@categoryId", projectCategory.CategoryId);
                        command.Parameters.AddWithValue("@projectId", projectCategory.ProjectId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Remove(int id)
        {
            string query = "delete from ProjectCategory where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }
    }
}
