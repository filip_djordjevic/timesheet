﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.DAL
{
    public class TeamMemberRepository : ITeamMemberRepository
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["TimeSheetDb"].ConnectionString;

        public IEnumerable<TeamMember> GetAll()
        {
            string query = "select Id, Name, Username, HoursPerWeek, Email, IsActive, IsAdmin from TeamMembers";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var teamMembers = new List<TeamMember>();
                            while (reader.Read())
                            {
                                var teamMember = new TeamMember
                                {
                                    Id = Convert.ToInt32(reader.GetValue(0)),
                                    Name = Convert.ToString(reader.GetValue(1)),
                                    UserName = Convert.ToString(reader.GetValue(2)),
                                    HoursPerWeek = Convert.ToDecimal(reader.GetValue(3)),
                                    Email = Convert.ToString(reader.GetValue(4)),
                                    IsActive = Convert.ToBoolean(reader.GetValue(5)),
                                    IsAdmin = Convert.ToBoolean(reader.GetValue(6))
                                };
                                teamMembers.Add(teamMember);
                            }
                            return teamMembers;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public TeamMember GetBy(int id)
        {
            var teamMember = new TeamMember();
            string query = "select Id, Name, Username, HoursPerWeek, Email, IsActive, IsAdmin from TeamMembers where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                teamMember.Id = Convert.ToInt32(reader.GetValue(0));
                                teamMember.Name = Convert.ToString(reader.GetValue(1));
                                teamMember.UserName = Convert.ToString(reader.GetValue(2));
                                teamMember.HoursPerWeek = Convert.ToDecimal(reader.GetValue(3));
                                teamMember.Email = Convert.ToString(reader.GetValue(4));
                                teamMember.IsActive = Convert.ToBoolean(reader.GetValue(5));
                                teamMember.IsAdmin = Convert.ToBoolean(reader.GetValue(6));
                            }
                            return teamMember;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Add(TeamMember teamMember)
        {
            string query = "insert into TeamMembers(Name, Username, HoursPerWeek, Email, IsActive, IsAdmin) values (@name, @userName, @hours, @email, @isActive, @isAdmin)";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@name", teamMember.Name);
                        command.Parameters.AddWithValue("@userName", teamMember.UserName);
                        command.Parameters.AddWithValue("@hours", teamMember.HoursPerWeek);
                        command.Parameters.AddWithValue("@email", teamMember.Email);
                        command.Parameters.AddWithValue("@isActive", teamMember.IsActive);
                        command.Parameters.AddWithValue("@isAdmin", teamMember.IsAdmin);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Update(TeamMember teamMember)
        {
            string query = "update TeamMembers set Name = @name, Username = @userName, HoursPerWeek = @hours, Email = @email, IsActive = @isActive, IsAdmin = @isAdmin where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", teamMember.Id);
                        command.Parameters.AddWithValue("@name", teamMember.Name);
                        command.Parameters.AddWithValue("@userName", teamMember.UserName);
                        command.Parameters.AddWithValue("@hours", teamMember.HoursPerWeek);
                        command.Parameters.AddWithValue("@email", teamMember.Email);
                        command.Parameters.AddWithValue("@isActive", teamMember.IsActive);
                        command.Parameters.AddWithValue("@isAdmin", teamMember.IsAdmin);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Remove(int id)
        {
            string query = "delete from TeamMembers where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }
    }
}
