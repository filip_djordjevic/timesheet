﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.DAL
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["TimeSheetDb"].ConnectionString;

        public IEnumerable<Category> GetAll()
        {
            string query = "select Id, Name from Categories";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var categories = new List<Category>();
                            while (reader.Read())
                            {
                                var category = new Category
                                {
                                    Id = Convert.ToInt32(reader.GetValue(0)),
                                    Name = Convert.ToString(reader.GetValue(1))
                                };
                                categories.Add(category);
                            }
                            return categories;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public Category GetBy(int id)
        {
            var category = new Category();
            string query = "select Id, Name from Categories where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                category.Id = Convert.ToInt32(reader.GetValue(0));
                                category.Name = Convert.ToString(reader.GetValue(1));
                            }
                            return category;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Add(Category category)
        {
            string query = "insert into Categories(Name) values (@name)";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@name", category.Name);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Update(Category category)
        {
            string query = "update Categories set Name = @name where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", category.Id);
                        command.Parameters.AddWithValue("@name", category.Name);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Remove(int id)
        {
            string query = "delete from Categories where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }
    }
}
