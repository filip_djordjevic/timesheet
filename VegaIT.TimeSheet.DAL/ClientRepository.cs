﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.DAL
{
    public class ClientRepository : IClientRepository
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["TimeSheetDb"].ConnectionString;

        public IEnumerable<Client> GetAll()
        {
            string query = "select Id, Name, Address, City, ZipCode, CountryId from Clients";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var clients = new List<Client>();
                            while (reader.Read())
                            {
                                var client = new Client
                                {
                                    Id = Convert.ToInt32(reader.GetValue(0)),
                                    Name = Convert.ToString(reader.GetValue(1)),
                                    Address = Convert.ToString(reader.GetValue(2)),
                                    City = Convert.ToString(reader.GetValue(3)),
                                    ZipCode = Convert.ToString(reader.GetValue(4)),
                                    CountryId = Convert.ToInt32(reader.GetValue(5))
                                };
                                clients.Add(client);
                            }
                            return clients;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public Client GetBy(int id)
        {
            var client = new Client();
            string query = "select Id, Name, Address, City, ZipCode, CountryId from Clients where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                client.Id = Convert.ToInt32(reader.GetValue(0));
                                client.Name = Convert.ToString(reader.GetValue(1));
                                client.Address = Convert.ToString(reader.GetValue(2));
                                client.City = Convert.ToString(reader.GetValue(3));
                                client.ZipCode = Convert.ToString(reader.GetValue(4));
                                client.CountryId = Convert.ToInt32(reader.GetValue(5));
                            }
                            return client;
                        }
                    }
                    catch(SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Add(Client client)
        {
            string query = "insert into Clients(Name, Address, City, ZipCode, CountryId) values (@name, @address, @city, @zipCode, @countryId)";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@name", client.Name);
                        command.Parameters.AddWithValue("@address", client.Address);
                        command.Parameters.AddWithValue("@city", client.City);
                        command.Parameters.AddWithValue("@zipCode", client.ZipCode);
                        command.Parameters.AddWithValue("@countryId", client.CountryId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Update(Client client)
        {
            string query = "update Clients set Name = @name, Address = @address, City = @city, ZipCode = @zipCode, CountryId = @countryId where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", client.Id);
                        command.Parameters.AddWithValue("@name", client.Name);
                        command.Parameters.AddWithValue("@address", client.Address);
                        command.Parameters.AddWithValue("@city", client.City);
                        command.Parameters.AddWithValue("@zipCode", client.ZipCode);
                        command.Parameters.AddWithValue("@countryId", client.CountryId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Remove(int id)
        {
            string query = "delete from Clients where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }
    }
}
