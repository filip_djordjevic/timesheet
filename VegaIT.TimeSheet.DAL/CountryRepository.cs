﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.DAL
{
    public class CountryRepository : ICountryRepository
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["TimeSheetDb"].ConnectionString;

        public IEnumerable<Country> GetAll()
        {
            string query = "select Id, Name from Countries";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var countries = new List<Country>();
                            while (reader.Read())
                            {
                                var country = new Country
                                {
                                    Id = Convert.ToInt32(reader.GetValue(0)),
                                    Name = Convert.ToString(reader.GetValue(1))
                                };
                                countries.Add(country);
                            }
                            return countries;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public Country GetBy(int id)
        {
            string query = "select Id, Name from Countries where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var country = new Country();
                            if (reader.Read())
                            {
                                country.Id = Convert.ToInt32(reader.GetValue(0));
                                country.Name = Convert.ToString(reader.GetValue(1));
                            }
                            return country;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Add(Country country)
        {
            string query = "insert into Countries(Name) values (@name)";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@name", country.Name);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Update(Country country)
        {
            string query = "update Countries set Name = @name where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", country.Id);
                        command.Parameters.AddWithValue("@name", country.Name);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Remove(int id)
        {
            string query = "delete from Countries where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }
    }
}
