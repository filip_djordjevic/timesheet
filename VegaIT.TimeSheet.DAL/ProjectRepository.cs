﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.DAL
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["TimeSheetDb"].ConnectionString;

        public IEnumerable<Project> GetAll()
        {
            string query = "select Id, Name, Description, Status, LeadId, ClientId from Projects";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var projects = new List<Project>();
                            while (reader.Read())
                            {
                                var project = new Project
                                {
                                    Id = Convert.ToInt32(reader.GetValue(0)),
                                    Name = Convert.ToString(reader.GetValue(1)),
                                    Description = Convert.ToString(reader.GetValue(2)),
                                    Status = Convert.ToInt32(reader.GetValue(3)),
                                    LeadId = Convert.ToInt32(reader.GetValue(4)),
                                    ClientId = Convert.ToInt32(reader.GetValue(5))
                                };
                                projects.Add(project);
                            }
                            return projects;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public Project GetBy(int id)
        {
            var project = new Project();
            string query = "select Id, Name, Description, Status, LeadId, ClientId from Projects where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                project.Id = Convert.ToInt32(reader.GetValue(0));
                                project.Name = Convert.ToString(reader.GetValue(1));
                                project.Description = Convert.ToString(reader.GetValue(2));
                                project.Status = Convert.ToInt32(reader.GetValue(3));
                                project.LeadId = Convert.ToInt32(reader.GetValue(4));
                                project.ClientId = Convert.ToInt32(reader.GetValue(5));
                            }
                            return project;
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Add(Project project)
        {
            string query = "insert into Projects(Name, Description, Status, LeadId, ClientId) values (@name, @description, @status, @leadId, @clientId)";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@name", project.Name);
                        command.Parameters.AddWithValue("@description", project.Description);
                        command.Parameters.AddWithValue("@status", project.Status);
                        command.Parameters.AddWithValue("@leadId", project.LeadId);
                        command.Parameters.AddWithValue("@clientId", project.ClientId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Update(Project project)
        {
            string query = "update Projects set Name = @name, Description = @description, Status = @status, LeadId = @leadId, clientId = @clientId where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", project.Id);
                        command.Parameters.AddWithValue("@name", project.Name);
                        command.Parameters.AddWithValue("@description", project.Description);
                        command.Parameters.AddWithValue("@status", project.Status);
                        command.Parameters.AddWithValue("@leadId", project.LeadId);
                        command.Parameters.AddWithValue("@clientId", project.ClientId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }

        public void Remove(int id)
        {
            string query = "delete from Projects where Id = @id";
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DataAccessException(ex.Message, ex);
                    }
                }
            }
        }
    }
}
