﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.Common;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.Business
{
    public class ProjectManager : IProjectManager
    {
        private readonly IProjectRepository projectRepository;

        public ProjectManager(IProjectRepository projectRepository)
        {
            this.projectRepository = projectRepository;
        }

        public void Create(Project project)
        {
            try
            {
                this.projectRepository.Add(project);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public void Delete(int id)
        {
            try
            {
                this.projectRepository.Remove(id);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public IEnumerable<Project> GetAll()
        {
            try
            {
                return this.projectRepository.GetAll();
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public IEnumerable<Project> GetBy(int pageNumber, string startingLetter, string searchText)
        {
            try
            {
                return this.projectRepository.GetAll()
                           .Where(p => string.IsNullOrEmpty(startingLetter) || p.Name.StartsWith(startingLetter))
                           .Where(p => string.IsNullOrEmpty(searchText) || p.Name.Contains(searchText))
                           .Skip((pageNumber - 1) * Constants.PageSize)
                           .Take(Constants.PageSize);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public void Update(Project project)
        {
            try
            {
                this.projectRepository.Update(project);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }
    }
}
