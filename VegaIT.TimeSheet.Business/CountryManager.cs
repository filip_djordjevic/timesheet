﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.Business
{
    public class CountryManager : ICountryManager
    {
        private readonly ICountryRepository countryRepository;

        public CountryManager(ICountryRepository countryRepository)
        {
            this.countryRepository = countryRepository;
        }

        public IEnumerable<Country> GetAll()
        {
            try
            {
                return this.countryRepository.GetAll();
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }
    }
}
