﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.Common;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.Business
{
    public class TeamMemberManager : ITeamMemberManager
    {
        private readonly ITeamMemberRepository teamMemberRepository;

        public TeamMemberManager(ITeamMemberRepository teamMemberRepository)
        {
            this.teamMemberRepository = teamMemberRepository;
        }

        public void Create(TeamMember teamMember)
        {
            try
            {
                this.teamMemberRepository.Add(teamMember);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public void Delete(int id)
        {
            try
            {
                this.teamMemberRepository.Remove(id);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public IEnumerable<TeamMember> GetAll()
        {
            try
            {
                return this.teamMemberRepository.GetAll();
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public IEnumerable<TeamMember> GetBy(int pageNumber)
        {
            try
            {
                return this.teamMemberRepository.GetAll()
                           .Skip((pageNumber - 1) * Constants.PageSize)
                           .Take(Constants.PageSize);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public void Update(TeamMember teamMember)
        {
            try
            {
                this.teamMemberRepository.Update(teamMember);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }
    }
}
