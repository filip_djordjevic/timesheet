﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.Business
{
    public class TimeSheetManager : ITimeSheetManager
    {
        private readonly ITimeSheetRepository timeSheetRepository;
        private readonly IProjectCategoryRepository projectCategoryRepository;

        public TimeSheetManager(ITimeSheetRepository timeSheetRepository, IProjectCategoryRepository projectCategoryRepository)
        {
            this.timeSheetRepository = timeSheetRepository;
            this.projectCategoryRepository = projectCategoryRepository;
        }

        public void Create(DAL.Contracts.Entities.TimeSheet timeSheet)
        {
            try
            {
                this.timeSheetRepository.Add(timeSheet);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public void Delete(int id)
        {
            try
            {
                this.timeSheetRepository.Remove(id);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public IEnumerable<DAL.Contracts.Entities.TimeSheet> GetAll()
        {
            return this.timeSheetRepository.GetAll();
        }

        public IEnumerable<DAL.Contracts.Entities.TimeSheet> GetBy(int teamMemberId, int clientId, int projectId, int categoryId, DateTime startDate, DateTime endDate)
        {
            try
            {
                List<DAL.Contracts.Entities.TimeSheet> reportTimeSheets = new List<DAL.Contracts.Entities.TimeSheet>();

                var timeSheets = this.timeSheetRepository.GetAll()
                           .Where(ts => teamMemberId == 0 || ts.TeamMemberId == teamMemberId)
                           .Where(ts => clientId == 0 || ts.ClientId == clientId)
                           .Where(ts => (startDate == null && endDate == null) || (ts.Date >= startDate && ts.Date <= endDate));

                var projectCategories = this.projectCategoryRepository.GetAll()
                            .Where(pc => (projectId == 0 || categoryId == 0) || (pc.CategoryId == categoryId && pc.ProjectId == projectId));

                foreach (ProjectCategory pc in projectCategories)
                {
                    reportTimeSheets.Add(timeSheets.Where(ts => ts.ProjectCategoryId == pc.Id).FirstOrDefault());
                }

                return reportTimeSheets;
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public void Update(DAL.Contracts.Entities.TimeSheet timeSheet)
        {
            try
            {
                this.timeSheetRepository.Update(timeSheet);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }
    }
}
