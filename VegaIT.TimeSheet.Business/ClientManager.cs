﻿using System.Collections.Generic;
using System.Linq;
using VegaIT.TimeSheet.Business.Contracts;
using VegaIT.TimeSheet.Business.Contracts.Exceptions;
using VegaIT.TimeSheet.Common;
using VegaIT.TimeSheet.DAL.Contracts.Entities;
using VegaIT.TimeSheet.DAL.Contracts.Exceptions;
using VegaIT.TimeSheet.DAL.Contracts.Repositories;

namespace VegaIT.TimeSheet.Business
{
    public class ClientManager : IClientManager
    {
        private readonly IClientRepository clientRepository;

        public ClientManager(IClientRepository clientRepository)
        {
            this.clientRepository = clientRepository;
        }

        public void Create(Client client)
        {
            try
            {
                this.clientRepository.Add(client);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public void Delete(int Id)
        {
            try
            {
                this.clientRepository.Remove(Id);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public IEnumerable<Client> GetAll()
        {
            try
            {
                return this.clientRepository.GetAll();
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public IEnumerable<Client> GetBy(int pageNumber, string startingLetter, string searchText)
        {
            try
            {
                return this.clientRepository.GetAll()
                           .Where(c => string.IsNullOrEmpty(startingLetter) || c.Name.StartsWith(startingLetter))
                           .Where(c => string.IsNullOrEmpty(searchText) || c.Name.Contains(searchText))
                           .Skip((pageNumber - 1) * Constants.PageSize)
                           .Take(Constants.PageSize);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }

        public void Update(Client client)
        {
            try
            {
                this.clientRepository.Update(client);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex.Message, ex);
            }
        }
    }
}