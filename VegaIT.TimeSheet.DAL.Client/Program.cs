﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.DAL.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            #region CategoriesTest
            var categoriesRepository = new CategoryRepository();

            /*var category = categoriesRepository.GetBy(1);

            Console.WriteLine($"Category Id: {category.Id}; Name: {category.Name}");

            var categoryToAdd = new Category()
            {
                Id = 2,
                Name = "FrontEnd Development"
            };

            categoriesRepository.Update(categoryToAdd);

            categoriesRepository.Remove(2);

            foreach (var category in categoriesRepository.GetAll())
            {
                Console.WriteLine($"Category Id: {category.Id}; Name: {category.Name}");
            }*/
            #endregion

            #region CountriesTest
            var countriesRepository = new CountryRepository();

            /*var countryToAdd = new Country()
            {
                Id = 1,
                Name = "Italy"
            };

            countriesRepository.Add(countryToAdd);

            var specificCountry = countriesRepository.GetBy(1);

            Console.WriteLine($"Country Id: {specificCountry.Id}; Name: {specificCountry.Name}");

            var anotherCountryToAdd = new Country()
            {
                Id = 2,
                Name = "Austria"
            };

            var countryToUpdate = new Country()
            {
                Id = 2,
                Name = "Hungary"
            };

            countriesRepository.Add(anotherCountryToAdd);

            foreach (var country in countriesRepository.GetAll())
            {
                Console.WriteLine($"Country Id: {country.Id}; Name: {country.Name}");
            }

            countriesRepository.Update(countryToUpdate);

            countriesRepository.Remove(2);

            foreach (var country in countriesRepository.GetAll())
            {
                Console.WriteLine($"Country Id: {country.Id}; Name: {country.Name}");
            }*/
            #endregion

            #region TeamMembersTest
            var teamMembersRepository = new TeamMemberRepository();

            /*var teamMemberToAdd = new TeamMember()
            {
                Id = 1,
                Name = "Filip Djordjevic",
                UserName = "FikiLauda",
                HoursPerWeek = 40,
                Email = "filip.cofi.djordjevic@gmail.com",
                IsActive = true,
                IsAdmin = false
            };

            teamMembersRepository.Add(teamMemberToAdd);

            var specificMember = teamMembersRepository.GetBy(1);

            Console.WriteLine($"TeamMember\nId: {specificMember.Id}; Name: {specificMember.Name}; Username: {specificMember.UserName}\nHours per week: {specificMember.HoursPerWeek}; "+
                                $"E-mail: {specificMember.Email}; Active: {specificMember.IsActive}; Admin: {specificMember.IsAdmin}");

            var anotherTeamMemberToAdd = new TeamMember()
            {
                Id = 2,
                Name = "Admin Adminovic",
                UserName = "Admin",
                HoursPerWeek = 40,
                Email = "admin@admin.com",
                IsActive = true,
                IsAdmin = true
            };

            var teamMemberToUpdate = new TeamMember()
            {
                Id = 2,
                Name = "Admin Adminic",
                UserName = "Admin",
                HoursPerWeek = 40,
                Email = "admin@admin-mail.com",
                IsActive = true,
                IsAdmin = true
            };

            teamMembersRepository.Add(anotherTeamMemberToAdd);

            teamMembersRepository.Update(teamMemberToUpdate);

            foreach (var teamMember in teamMembersRepository.GetAll())
            {
                Console.WriteLine($"TeamMember\nId: {teamMember.Id}; Name: {teamMember.Name}; Username: {teamMember.UserName}\nHours per week: {teamMember.HoursPerWeek}; " +
                                $"E-mail: {teamMember.Email}; Active: {teamMember.IsActive}; Admin: {teamMember.IsAdmin}");
            }

            teamMembersRepository.Remove(2);

            foreach (var teamMember in teamMembersRepository.GetAll())
            {
                Console.WriteLine($"TeamMember\nId: {teamMember.Id}; Name: {teamMember.Name}; Username: {teamMember.UserName}\nHours per week: {teamMember.HoursPerWeek}; " +
                                $"E-mail: {teamMember.Email}; Active: {teamMember.IsActive}; Admin: {teamMember.IsAdmin}");
            }*/
            #endregion

            #region ClientsTest
            var clientsRepository = new ClientRepository();

            /*var clientToAdd = new Contracts.Entities.Client()
            {
                Id = 11,
                Name = "Scuderia Ferrari F1",
                Address = "Via Alfredo Dino Ferrari,43",
                City = "Maranello",
                ZipCode = "41053",
                CountryId = 1
            };

            clientsRepository.Add(clientToAdd);

            var specificClient = clientsRepository.GetBy(11);

            Console.WriteLine($"Client\nId: {specificClient.Id}; Name: {specificClient.Name}\nAddress: {specificClient.Address}; City: {specificClient.City};" +
                                $" ZipCode: {specificClient.ZipCode}; Country: {countriesRepository.GetBy(specificClient.CountryId).Name}");

            var anotherClientToAdd = new Contracts.Entities.Client()
            {
                Id = 12,
                Name = "ClientName",
                Address = "ClientAddress",
                City = "ClientCity",
                ZipCode = "ZipCode",
                CountryId = 1
            };

            var clientToUpdate = new Contracts.Entities.Client()
            {
                Id = 12,
                Name = "Scuderia Toro Rosso F1",
                Address = "Via Boaria, 229",
                City = "Faenza",
                ZipCode = "48018",
                CountryId = 1
            };

            clientsRepository.Add(anotherClientToAdd);

            clientsRepository.Update(clientToUpdate);

            foreach (var client in clientsRepository.GetAll())
            {
                Console.WriteLine($"Client\nId: {client.Id}; Name: {client.Name}\nAddress: {client.Address}; City: {client.City};" +
                                $" ZipCode: {client.ZipCode}; Country: {countriesRepository.GetBy(client.CountryId).Name}");
            }

            clientsRepository.Remove(12);

            foreach (var client in clientsRepository.GetAll())
            {
                Console.WriteLine($"Client\nId: {client.Id}; Name: {client.Name}\nAddress: {client.Address}; City: {client.City};" +
                                $" ZipCode: {client.ZipCode}; Country: {countriesRepository.GetBy(client.CountryId).Name}");
            }*/
            #endregion

            #region ProjectsTest
            var projectsRepository = new ProjectRepository();

            /*var projectsToAdd = new Project()
            {
                Id = 4,
                Name = "Project1",
                Description = "Description of Project1",
                Status = 1,
                LeadId = 1,
                ClientId = 11
            };

            projectsRepository.Add(projectsToAdd);

            var specificProject = projectsRepository.GetBy(4);

            Console.WriteLine($"Client\nId: {specificProject.Id}; Name: {specificProject.Name}\nDescription: {specificProject.Description}; Status: {specificProject.Status};" +
                                $" Lead: {teamMembersRepository.GetBy(specificProject.LeadId).Name}; Client: {clientsRepository.GetBy(specificProject.ClientId).Name}");

            var anotherProjectsToAdd = new Project()
            {
                Id = 5,
                Name = "Project2",
                Description = "Description of Project2",
                Status = 2,
                LeadId = 1,
                ClientId = 11
            };

            var projectToUpdate = new Project()
            {
                Id = 5,
                Name = "Project 2 Name",
                Description = "Description of Project 2",
                Status = 2,
                LeadId = 1,
                ClientId = 11
            };

            projectsRepository.Add(anotherProjectsToAdd);

            projectsRepository.Update(projectToUpdate);

            foreach (var project in projectsRepository.GetAll())
            {
                Console.WriteLine($"Client\nId: {project.Id}; Name: {project.Name}\nDescription: {project.Description}; Status: {project.Status};" +
                                $" Lead: {teamMembersRepository.GetBy(project.LeadId).Name}; Client: {clientsRepository.GetBy(project.ClientId).Name}");
            }

            projectsRepository.Remove(2);

            foreach (var project in projectsRepository.GetAll())
            {
                Console.WriteLine($"Client\nId: {project.Id}; Name: {project.Name}\nDescription: {project.Description}; Status: {project.Status};" +
                                $" Lead: {teamMembersRepository.GetBy(project.LeadId).Name}; Client: {clientsRepository.GetBy(project.ClientId).Name}");
            }*/
            #endregion

            #region ProjectCategoryTest
            var projectCategoryRepository = new ProjectCategoryRepository();

            /*var projectCategoryToAdd = new ProjectCategory()
            {
                Id = 1,
                CategoryId = 1,
                ProjectId = 4
            };

            projectCategoryRepository.Add(projectCategoryToAdd);

            var specificProjectCategory = projectCategoryRepository.GetBy(1);

            Console.WriteLine($"ProjectCategory Id: {specificProjectCategory.Id}; Category: {categoriesRepository.GetBy(specificProjectCategory.CategoryId).Name}; Project: {projectsRepository.GetBy(specificProjectCategory.ProjectId).Name}");

            var anotheProjectCategoryToAdd = new ProjectCategory()
            {
                Id = 2,
                CategoryId = 1,
                ProjectId = 5
            };

            var projectCategoryToUpdate = new ProjectCategory()
            {
                Id = 2,
                CategoryId = 1,
                ProjectId = 4
            };

            projectCategoryRepository.Add(anotheProjectCategoryToAdd);

            projectCategoryRepository.Update(projectCategoryToUpdate);

            foreach (var projectCategory in projectCategoryRepository.GetAll())
            {
                Console.WriteLine($"ProjectCategory Id: {projectCategory.Id}; Category: {categoriesRepository.GetBy(projectCategory.CategoryId).Name}; Project: {projectsRepository.GetBy(projectCategory.ProjectId).Name}");
            }

            projectCategoryRepository.Remove(2);

            foreach (var projectCategory in projectCategoryRepository.GetAll())
            {
                Console.WriteLine($"ProjectCategory Id: {projectCategory.Id}; Category: {categoriesRepository.GetBy(projectCategory.CategoryId).Name}; Project: {projectsRepository.GetBy(projectCategory.ProjectId).Name}");
            }*/
            #endregion

            #region TimeSheetsTest
            var timeSheetRepository = new TimeSheetRepository();

            /*var timeSheetToAdd = new Contracts.Entities.TimeSheet()
            {
                Id = 1,
                Description = "Time Sheet Description",
                Time = 5.5m,
                Overtime = 0,
                Date = DateTime.Now,
                TeamMemberId = 1,
                ClientId = 11,
                ProjectCategoryId = 1
            };

            timeSheetRepository.Add(timeSheetToAdd);

            var specificTimeSheet = timeSheetRepository.GetBy(1);

            Console.WriteLine($"TimeSheet\nId: {specificTimeSheet.Id}; Description: {specificTimeSheet.Description}; Time: {specificTimeSheet.Time}; Overtime: {specificTimeSheet.Overtime}; Date: {specificTimeSheet.Date}\n"+
                                $"Team Member: {teamMembersRepository.GetBy(specificTimeSheet.TeamMemberId).Name}; Client: {clientsRepository.GetBy(specificTimeSheet.ClientId).Name}\n"+
                                $"Project: {projectsRepository.GetBy(projectCategoryRepository.GetBy(specificTimeSheet.ProjectCategoryId).ProjectId).Name}; Category: {categoriesRepository.GetBy(projectCategoryRepository.GetBy(specificTimeSheet.ProjectCategoryId).CategoryId).Name};");

            var anotherTimeSheetToAdd = new Contracts.Entities.TimeSheet()
            {
                Id = 2,
                Description = "Time Sheet 2 Description",
                Time = 3.5m,
                Overtime = 1,
                Date = DateTime.Now,
                TeamMemberId = 1,
                ClientId = 11,
                ProjectCategoryId = 1
            };

            var timeSheetToUpdate = new Contracts.Entities.TimeSheet()
            {
                Id = 2,
                Description = "Time Sheet 2 Description",
                Time = 6.5m,
                Overtime = 1,
                Date = anotherTimeSheetToAdd.Date,
                TeamMemberId = 1,
                ClientId = 11,
                ProjectCategoryId = 1
            };

            timeSheetRepository.Add(anotherTimeSheetToAdd);

            timeSheetRepository.Update(timeSheetToUpdate);

            foreach (var timeSheet in timeSheetRepository.GetAll())
            {
                Console.WriteLine($"TimeSheet\nId: {timeSheet.Id}; Description: {timeSheet.Description}; Time: {timeSheet.Time}; Overtime: {timeSheet.Overtime}; Date: {timeSheet.Date}\n" +
                                $"Team Member: {teamMembersRepository.GetBy(timeSheet.TeamMemberId).Name}; Client: {clientsRepository.GetBy(timeSheet.ClientId).Name}\n" +
                                $"Project: {projectsRepository.GetBy(projectCategoryRepository.GetBy(timeSheet.ProjectCategoryId).ProjectId).Name}; Category: {categoriesRepository.GetBy(projectCategoryRepository.GetBy(timeSheet.ProjectCategoryId).CategoryId).Name};");
            }

            timeSheetRepository.Remove(2);

            foreach (var timeSheet in timeSheetRepository.GetAll())
            {
                Console.WriteLine($"TimeSheet\nId: {timeSheet.Id}; Description: {timeSheet.Description}; Time: {timeSheet.Time}; Overtime: {timeSheet.Overtime}; Date: {timeSheet.Date}\n" +
                                $"Team Member: {teamMembersRepository.GetBy(timeSheet.TeamMemberId).Name}; Client: {clientsRepository.GetBy(timeSheet.ClientId).Name}\n" +
                                $"Project: {projectsRepository.GetBy(projectCategoryRepository.GetBy(timeSheet.ProjectCategoryId).ProjectId).Name}; Category: {categoriesRepository.GetBy(projectCategoryRepository.GetBy(timeSheet.ProjectCategoryId).CategoryId).Name};");
            }*/
            #endregion

            Console.ReadLine();
        }
    }
}
