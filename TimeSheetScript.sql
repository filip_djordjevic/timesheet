-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-06-19 12:18:53.578

-- tables
-- Table: Categories
CREATE TABLE Categories (
    Id int  NOT NULL,
    Name nvarchar(30)  NOT NULL,
    CONSTRAINT Categories_pk PRIMARY KEY  (Id)
);

-- Table: Clients
CREATE TABLE Clients (
    Id int  NOT NULL,
    Name nvarchar(30)  NOT NULL,
    Address nvarchar(50)  NOT NULL,
    City nvarchar(30)  NOT NULL,
    ZipCode nvarchar(10)  NOT NULL,
    CountryId int  NOT NULL,
    CONSTRAINT Clients_pk PRIMARY KEY  (Id)
);

-- Table: Countries
CREATE TABLE Countries (
    Id int  NOT NULL,
    Name nvarchar(30)  NOT NULL,
    CONSTRAINT Countries_pk PRIMARY KEY  (Id)
);

-- Table: ProjectCategory
CREATE TABLE ProjectCategory (
    Id int  NOT NULL,
    CategoryId int  NOT NULL,
    ProjectId int  NOT NULL,
    CONSTRAINT ProjectCategory_pk PRIMARY KEY  (Id)
);

-- Table: Projects
CREATE TABLE Projects (
    Id int  NOT NULL,
    Name nvarchar(30)  NOT NULL,
    Description nvarchar(100)  NULL,
    Status int  NOT NULL,
    LeadId int  NOT NULL,
    ClientId int  NOT NULL,
    CONSTRAINT Projects_pk PRIMARY KEY  (Id)
);

-- Table: TeamMembers
CREATE TABLE TeamMembers (
    Id int  NOT NULL,
    Name nvarchar(30)  NOT NULL,
    Username nvarchar(20)  NOT NULL,
    HoursPerWeek decimal(10,2)  NOT NULL,
    Email nvarchar(50)  NOT NULL,
    IsActive bit  NOT NULL,
    IsAdmin bit  NOT NULL,
    CONSTRAINT TeamMembers_pk PRIMARY KEY  (Id)
);

-- Table: TimeSheets
CREATE TABLE TimeSheets (
    Id int  NOT NULL,
    Description nvarchar(100)  NULL,
    Time decimal(10,2)  NOT NULL,
    Overtime decimal(10,2)  NULL,
    Date date  NOT NULL,
    TeamMemberId int  NOT NULL,
    ClientId int  NOT NULL,
    ProjectCategoryId int  NOT NULL,
    CONSTRAINT TimeSheets_pk PRIMARY KEY  (Id)
);

-- foreign keys
-- Reference: Clients_Countries (table: Clients)
ALTER TABLE Clients ADD CONSTRAINT Clients_Countries
    FOREIGN KEY (CountryId)
    REFERENCES Countries (Id);

-- Reference: ProjectCategory_Category (table: ProjectCategory)
ALTER TABLE ProjectCategory ADD CONSTRAINT ProjectCategory_Category
    FOREIGN KEY (CategoryId)
    REFERENCES Categories (Id);

-- Reference: ProjectCategory_Projects (table: ProjectCategory)
ALTER TABLE ProjectCategory ADD CONSTRAINT ProjectCategory_Projects
    FOREIGN KEY (ProjectId)
    REFERENCES Projects (Id);

-- Reference: Projects_Clients (table: Projects)
ALTER TABLE Projects ADD CONSTRAINT Projects_Clients
    FOREIGN KEY (ClientId)
    REFERENCES Clients (Id);

-- Reference: Projects_TeamMembers (table: Projects)
ALTER TABLE Projects ADD CONSTRAINT Projects_TeamMembers
    FOREIGN KEY (LeadId)
    REFERENCES TeamMembers (Id);

-- Reference: TimeSheetInputs_Clients (table: TimeSheets)
ALTER TABLE TimeSheets ADD CONSTRAINT TimeSheetInputs_Clients
    FOREIGN KEY (ClientId)
    REFERENCES Clients (Id);

-- Reference: TimeSheetInputs_TeamMembers (table: TimeSheets)
ALTER TABLE TimeSheets ADD CONSTRAINT TimeSheetInputs_TeamMembers
    FOREIGN KEY (TeamMemberId)
    REFERENCES TeamMembers (Id);

-- Reference: TimeSheet_ProjectCategory (table: TimeSheets)
ALTER TABLE TimeSheets ADD CONSTRAINT TimeSheet_ProjectCategory
    FOREIGN KEY (ProjectCategoryId)
    REFERENCES ProjectCategory (Id);

-- End of file.