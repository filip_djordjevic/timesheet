﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.DAL.Contracts.Repositories
{
    public interface ITeamMemberRepository
    {
        IEnumerable<TeamMember> GetAll();

        TeamMember GetBy(int id);

        void Add(TeamMember teamMember);

        void Update(TeamMember teamMember);

        void Remove(int id);
    }
}
