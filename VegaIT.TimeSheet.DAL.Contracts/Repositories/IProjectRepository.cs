﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.DAL.Contracts.Repositories
{
    public interface IProjectRepository
    {
        IEnumerable<Project> GetAll();

        Project GetBy(int id);

        void Add(Project project);

        void Update(Project project);

        void Remove(int id);
    }
}
