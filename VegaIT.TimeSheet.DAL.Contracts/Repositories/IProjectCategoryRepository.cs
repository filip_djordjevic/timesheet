﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.DAL.Contracts.Repositories
{
    public interface IProjectCategoryRepository
    {
        IEnumerable<ProjectCategory> GetAll();

        ProjectCategory GetBy(int id);

        void Add(ProjectCategory projectCategory);

        void Update(ProjectCategory projectCategory);

        void Remove(int id);
    }
}
