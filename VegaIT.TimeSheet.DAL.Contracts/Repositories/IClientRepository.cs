﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.DAL.Contracts.Repositories
{
    public interface IClientRepository
    {
        IEnumerable<Client> GetAll();

        Client GetBy(int id);

        void Add(Client client);

        void Update(Client client);

        void Remove(int id);
    }
}
