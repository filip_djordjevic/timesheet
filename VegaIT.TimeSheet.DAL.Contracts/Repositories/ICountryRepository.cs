﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.DAL.Contracts.Repositories
{
    public interface ICountryRepository
    {
        IEnumerable<Country> GetAll();

        Country GetBy(int id);

        void Add(Country country);

        void Update(Country country);

        void Remove(int id);
    }
}
