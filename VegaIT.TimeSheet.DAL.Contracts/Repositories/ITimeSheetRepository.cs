﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VegaIT.TimeSheet.DAL.Contracts.Entities;

namespace VegaIT.TimeSheet.DAL.Contracts.Repositories
{
    public interface ITimeSheetRepository
    {
        IEnumerable<Entities.TimeSheet> GetAll();

        Entities.TimeSheet GetBy(int id);

        void Add(Entities.TimeSheet timeSheet);

        void Update(Entities.TimeSheet timeSheet);

        void Remove(int id);
    }
}