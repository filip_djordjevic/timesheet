﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VegaIT.TimeSheet.DAL.Contracts.Entities
{
    public class TimeSheet
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public decimal Time { get; set; }

        public decimal Overtime { get; set; }

        public DateTime Date { get; set; }

        public int TeamMemberId { get; set; }

        public int ClientId { get; set; }

        public int ProjectCategoryId { get; set; }
    }
}
