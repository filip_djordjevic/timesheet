﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VegaIT.TimeSheet.DAL.Contracts.Entities
{
    public class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
