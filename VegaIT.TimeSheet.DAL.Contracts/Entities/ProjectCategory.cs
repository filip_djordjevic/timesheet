﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VegaIT.TimeSheet.DAL.Contracts.Entities
{
    public class ProjectCategory
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public int ProjectId { get; set; }
    }
}
