﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VegaIT.TimeSheet.DAL.Contracts.Entities
{
    public class TeamMember
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string UserName { get; set; }

        public decimal HoursPerWeek { get; set; }

        public string Email { get; set; }

        public bool IsActive { get; set; }

        public bool IsAdmin { get; set; }
    }
}
