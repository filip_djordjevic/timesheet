﻿using Autofac;
using System.Linq;
using VegaIT.TimeSheet.Business;
using VegaIT.TimeSheet.DAL;

namespace VegaIT.TimeSheet.Infrastructure
{
    public class AutofacConfig
    {
        public static ContainerBuilder RegisterTypes()
        {
            var builder = new ContainerBuilder();

            // Register types that expose interfaces...
            var dataAccess = typeof(ClientRepository).Assembly;
            var business = typeof(ClientManager).Assembly;

            builder.RegisterAssemblyTypes(dataAccess)
                   .Where(t => t.Name.EndsWith("Repository"))
                   .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(business)
                   .Where(t => t.Name.EndsWith("Manager"))
                   .AsImplementedInterfaces();

            return builder;
        }
    }
}